> Nota preliminar: tots els comentaris de la plantilla són informatius i han de desaparéixer en la versió entregable

# SISTEMA ACME - GLOSSARI #


> El glossari llista els termes del domini que són claus en el sistema. 
> 
> El glossari **no** ha d'incloure termes propis de l'enginyeria del software
> 
> Els termes del glossari han d'estar ordenats alfabèticament
> 
> Si hi ha abreviatures, han d'aparéixer fent referència al terme complet, p.e.: GPS: v. Global Positioning System ("v." vol dir "vegeu")
>
> Igualment, si hi ha sinònims, heu d'elegir un terme de referència i la resta, que en faci referència usant també el "v."


Per cada terme:

- Banc del temps: iniciativa comunitària d’intercanvi de temps, que emula el funcionament d’un banc però l’intercanvi consisteix en el temps dedicat a serveis a les persones. Funciona de manera bilateral
- Terme 1: definició
- Terme 2: definició
- ...
