> Nota preliminar: tots els comentaris de la plantilla són informatius i han de desaparéixer en la versió entregable

# SISTEMA ACME - PLA D'ITERACIÓ *la que sigui* #

> El propòsit del document d'iteració és descriure en detall la planificació de la següent iteració. Tindrem, doncs, un document per cada iteració. El document es fa tot just abans d'iniciar la iteració

## 1. PRESENTACIÓ DE LA ITERACIÓ ##

> Resumir la informació de la iteració que més o menys ja està en el pla de desenvolupament del projecte: dates, esforç per rol, etc.

## 2. COBERTURA DE CASOS D'ÚS ##

> Poseu l'estat en què penseu es trobarà cada cas d'ús en acabar la iteració. Ha de ser coherent amb l'esforç assignat a la iteració

## 3. ACTIVITATS ##

> Llisteu les activitats de la iteració, juntament amb el temps esperat d'execució. Establiu les precedències entre activitats
 
## 4. DIAGRAMA DE GANTT ##

> Feu el diagrama de Gantt amb tota la informació necessària (dates, personal, fites, precedències, ...). Opcionalment, podeu fer el diagrama de PERT per discernir el temps mínim i màxim d'execució