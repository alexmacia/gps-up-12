> Nota preliminar: tots els comentaris de la plantilla són informatius i han de desaparéixer en la versió entregable

# SISTEMA ACME - VISIÓ #

> El propòsit del document de visió és recollir, analitzar i definir necesitats i capacitats d'alt nivell del sistema objecte del projecte

En un moment de crisi econòmica com l’actual, la societat s’organitza per fer un millor món, socialment més just, solidari i per millorar l’entorn en el qual vivim.  És per això doncs que per contribuir en aquesta economia col·laborativa sorgida, la nostra aportació, és la creació d’un banc del temps, centrada en necessitats informàtiques a la ciutat de Barcelona. Per a què això funcioni hem de tenir usuaris de dues parts: estudiants de la FIB (Facultat d’Informàtica de Barcelona) i ciutadans de la ciutat. L’objectiu és que els usuaris intercanviïn coneixement, sense cap tipus de transacció econòmica pel mig, si no que la transacció que hi ha entre els diferents usuaris sigui en hores (temps lliure).  Els estudiants d’informàtica aportaran el seu coneixement en el món de les TIC, moltes persones no natives en aquest àmbit, poden necessitar ajuda en moments determinats: quin ordinador comprar, reparació d’equips per averia, resolució de problemes com virus, etc. Els estudiants a canvi podran rebre altres serveis que la resta dels usuaris prestin i siguin del seu interès. Aquesta plataforma que s’usarà mitjançant una aplicació mòbil (l’objectiu d’aquest projecte), per tal que tots els usuaris puguin accedir-hi d’una manera còmode i ràpida.

## 2. EL PROBLEMA ##

> Descrivim el problema que volem resoldre amb èmfasi en el seu impacte (sobre persones, la societat, l'economia, ...)

El que es pretén resoldre amb la creació d’aquesta plataforma d’intercanvi d’hores (banc del temps) és que la societat aprengui a fer coses de les quals altres persones són expertes sense necessitat de pagar ningú amb diners, sinó que aquest intercanvi sigui a nivell intel·lectual. Això a part de suposar un canvi de mentalitat,d’una societat més individual a una que aposti pel col·lectiu, fa que aquesta evolucioni cap a una societat més justa, incrementa la confiança de les persones que hi estan involucrades, també s’incrementa l’autonomia de les mateixes ja que gràcies a aquest intercanvi s’adquireixen nous coneixements i fan que puguin ser més feliços.
En l’actualitat serveix a més a més per un gran col·lectiu de persones que en moments de crisi com els actuals, amb un gran nombre d’aturats, tinguin el mateix dret a rebre ajuda, encara que no puguin pagar pel servei rebut, donant a canvi allò que ells coneixen i els prestadors de l’altre part del servei puguin no saber o ser-ne inexperts.


## 3. PARTS INTERESSADES ##

> Descriviu totes les parts interessades en el sistema

1. *Part interessada 1*. Descripció d'interès. Responsabilitats per a què el sistema funcioni (si n'hi ha)
2. *Part interessada 2*. Descripció d'interès. Responsabilitats per a què el sistema funcioni (si n'hi ha)
3. ...

## 4. EL PRODUCTE ##

> Aquesta secció descriu en alt nivell les capacitats del producte, supòsits de funcionament, dependències sobre altres sistemes, ...

### 4.1. Perspectiva del producte ###
> Descriviu de quin tipus de producte estem parlant: un sistema d'informació, una app per a mòvil, un servei, un subsistema integrat en un sistema més gran, ...
 
> Resumiu la informació en un dibuix:

![](http://www.dittoditto.com/img/screenshots/soft-arch.gif)

### 4.2. Descripció del producte ###
> Enumereu molt breument les funcionalitats més importants (el detall estarà en altre document, el d'especificació):

1. *Funcionalitat 1*. Breu descripció 
2. *Funcionalitat 2*. Breu descripció 
3. ...
 
### 4.3. Supòsits de funcionament ###
> Enumereu els supòsits per a que el producti satisfagui la seva funcionalitat:

1. *Supòsit 1*. Descripció 
2. *Supòsit 2*. Descripció 
3. ...
 
### 4.4. Dependències sobre altres sistemes ###
> Enumereu les dependències del producte sobre altres sistemes existents:

1. *Dependència 1*. Descripció 
2. *Dependència 2*. Descripció 
3. ...
  
### 4.5. Altres requisits ###
> Aquí ens referim només als requisits no funcionals (usabilitat, eficiència, ...). Poseu els més importants amb una molt breu descripció:

1. *Requisit 1*. Descripció 
2. *Requisit 2*. Descripció 
3. ...

## 5. RECURSOS ##

> Si heu usat webs, documents, articles, etc., per basar el vostre document, enumereu aquí les referències tal i com es mostra aball. Des de la resta del document, cal referenciar l'estudi amb el seu ID entre claudàtors, "[*id*]"

[1] recurs 1

[2] recurs 2

[3] ...
