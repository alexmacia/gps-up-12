> Nota preliminar: tots els comentaris de la plantilla són informatius i han de desaparéixer en la versió entregable

# SISTEMA ACME - VISIÓ #


> El propòsit del document de visió és recollir, analitzar i definir necesitats i capacitats d'alt nivell del sistema objecte del projecte


## 1. INTRODUCCIÓ ##

> Breu motivació i presentació del sistema. No hauria d'ocupar més de 250 paraules

## 2. EL PROBLEMA ##

> Descrivim el problema que volem resoldre amb èmfasi en el seu impacte (sobre persones, la societat, l'economia, ...)

## 3. PARTS INTERESSADES ##

> Descriviu totes les parts interessades en el sistema

1. *Part interessada 1*. Descripció d'interès. Responsabilitats per a què el sistema funcioni (si n'hi ha)
2. *Part interessada 2*. Descripció d'interès. Responsabilitats per a què el sistema funcioni (si n'hi ha)
3. ...

## 4. EL PRODUCTE ##

> Aquesta secció descriu en alt nivell les capacitats del producte, supòsits de funcionament, dependències sobre altres sistemes, ...

### 4.1. Perspectiva del producte ###
> Descriviu de quin tipus de producte estem parlant: un sistema d'informació, una app per a mòvil, un servei, un subsistema integrat en un sistema més gran, ...
 
> Resumiu la informació en un dibuix:

![](http://www.dittoditto.com/img/screenshots/soft-arch.gif)

### 4.2. Descripció del producte ###
> Enumereu molt breument les funcionalitats més importants (el detall estarà en altre document, el d'especificació):

1. *Funcionalitat 1*. Breu descripció 
2. *Funcionalitat 2*. Breu descripció 
3. ...
 
### 4.3. Supòsits de funcionament ###
> Enumereu els supòsits per a que el producti satisfagui la seva funcionalitat:

1. *Supòsit 1*. Descripció 
2. *Supòsit 2*. Descripció 
3. ...
 
### 4.4. Dependències sobre altres sistemes ###
> Enumereu les dependències del producte sobre altres sistemes existents:

1. *Dependència 1*. Descripció 
2. *Dependència 2*. Descripció 
3. ...
  
### 4.5. Altres requisits ###
> Aquí ens referim només als requisits no funcionals (usabilitat, eficiència, ...). Poseu els més importants amb una molt breu descripció:

1. *Requisit 1*. Descripció 
2. *Requisit 2*. Descripció 
3. ...

## 5. RECURSOS ##

> Si heu usat webs, documents, articles, etc., per basar el vostre document, enumereu aquí les referències tal i com es mostra aball. Des de la resta del document, cal referenciar l'estudi amb el seu ID entre claudàtors, "[*id*]"

[1] recurs 1

[2] recurs 2

[3] ...
