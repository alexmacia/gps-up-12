> Nota preliminar: tots els comentaris de la plantilla són informatius i han de desaparéixer en la versió entregable

# SISTEMA ACME - CAS DE NEGOCI #

> El propòsit del document de cas de negoci proveir la informació necessària des d'un punt de vista de negoci per decidir si val la pena tirar endavant el projecte


## 1. DESCRIPCIÓ DEL PRODUCTE ##

> Breu resum (3-4 línies) de la visió del producte. Cal que sigui concordant amb el contingut del document de visió

## 2. CONTEXT DE NEGOCI ##

> Cal descriure: quin és el sector de negoci (telecomunicacions, banc, salut, ...); és un producte de consum intern, és per vendre a un client, és per fer-ne llicències, és per vendre-hi serveis, ...? Quina vida prevista té? etc.

## 3. OBJECTIUS DEL PRODUCTE ##

> Enumereu els objectius que pretèn cobrir el sistema

1. *Objectiu 1*. Descripció 
2. *Objectiu 2*. Descripció 
3. ...

## 4. RESTRICCIONS ##

> Enumereu les restriccions sobre les quals es desenvoluparà el producte. Poden ser restriccions de domini (abast), restriccions del projecte mateix (e.g., sobre l'equip de treball), restriccions tecnològiques, estàndards als què cal adherir-se, etc.

1. *Restricció 1*. Descripció 
2. *Restricció 2*. Descripció 
3. ...

## 5. PREVISIÓ FINANCERA ##

> Si el producte és per comercialitzar, el cas de negoci ha d'incloure un conjunt de supòsits sobre el projecte (p.e., nombre potencial de compradors/clients) i una previsió de guanys (a partir de quin moment començarem a recuperar la inversió, i quin serà el guany anual a partir d'aquell moment). Aquesta previsió necessitarà saber el cost del projecte, per la qual cosa aquest apartat no es pot completar fins tenir aquesta dada. A més, és la típica secció que s'anirà actualitzant contínuament (i.e., després de cada iteració UP)

## 6. RECURSOS ##

> Si heu usat webs, documents, articles, etc., per basar el vostre estudi, enumereu aquí les referències tal i com es mostra aball. Des de la resta del document, cal referenciar l'estudi amb el seu ID entre claudàtors, "[*id*]"

[1] recurs 1

[2] recurs 2

[3] ...